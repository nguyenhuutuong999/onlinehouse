﻿using OnlineHouse.App_Data;
using OnlineHouse.Code;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;

namespace OnlineHouse.Controllers
{

    public class HouseController : Controller
    {
        // GET: Zone
        private HouseManageDb context = new HouseManageDb();

        public ActionResult Index()
        {
            ViewBag.Message = Session["Inform"];
            Session["Inform"] = null;
            Session["UserID"] = "toinguyen";
            if (Session["UserID"] != null)
            {
                
                var UserID = Session["UserID"];
                var model = context.Houses.SqlQuery("select * from House where UserID = '" + UserID + "'").ToList<House>();
                return View(model);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
           

        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var house = new HouseDao().ViewDetail(id);

            return View(house);
        }


        /* Create ASP
        [HttpPost]
        public ActionResult Create(House house)
        {
            HouseManageDb context = new HouseManageDb();
            if (ModelState.IsValid)
            {
                var dao = new HouseDao();
                house.HouseID = dao.countAscending();
                house.UserID = (Session["UserID"]).ToString();


                //int id = dao.Insert(house);

                object[] sqlParams =
           {
                new SqlParameter("@HouseID", house.HouseID),
                 new SqlParameter("@UserID", house.UserID),
                  new SqlParameter("@NameHouse", house.NameHouse),
                 new SqlParameter("@Infor", house.Infor),
                 new SqlParameter("@Address", house.Address)

            };
                string id = context.Database.SqlQuery<string>("House_Create @HouseID, @UserID, @NameHouse, @Infor, @Address", sqlParams).SingleOrDefault();

                if (id == house.HouseID)
                {
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("", "Create Error");
                }

                
            }
            return View("Create");
        }*/

        [HttpPost]
        public ActionResult createHouse(House std)
        {
            var dao = new HouseDao();
            std.HouseID = dao.countAscending();
            std.UserID = (Session["UserID"]).ToString();
            std.Status = true;
            context.Houses.Add(std);
            context.SaveChanges();
            string message = "SUCCESS";
            return Json(new { Message = message, JsonRequestBehavior.AllowGet });

        }

        public JsonResult getHouse(string id)
        {
            List<House> houses = new List<House>();
            houses = context.Houses.ToList();
            return Json(houses, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult Edit(House house)
        {
            if (ModelState.IsValid)
            {
                var dao = new HouseDao();
                var result = dao.update(house);

                if (result)
                {
                    return RedirectToAction("Index", "House");
                }
                else
                {
                    ModelState.AddModelError("", "Update Error");
                }
            }
            return View("Edit");
        }

        [HttpDelete]
        public ActionResult Delete(int id)
        {
            new HouseDao().Delete(id);
            return RedirectToAction("Index", "House");
        }

        [HttpPost]
        public ActionResult UploadFiles()
        {
            // Checking no of files injected in Request object  
            if (Request.Files.Count > 0)
            {
                try
                {
                    //  Get all files from Request object  
                    for (int i = 0; i < Request.Files.Count; i++)
                    {
                        HttpPostedFileBase file = Request.Files[i]; //Uploaded file
                                                                    //Use the following properties to get file's name, size and MIMEType
                        int fileSize = file.ContentLength;
                        string fileName = file.FileName;
                        string mimeType = file.ContentType;
                        System.IO.Stream fileContent = file.InputStream;
                        //To save file, use SaveAs method

                        fileName = Path.Combine(Server.MapPath("~/ImageUpdate/"), fileName);
                        file.SaveAs(fileName);

                    }
                    return Json("Uploaded Image files");
                }
                catch (Exception ex)
                {
                    return Json("Error occurred. Error details: " + ex.Message);
                }
            }
            else
            {
                return Json("No files selected.");
            }
        }


        [ChildActionOnly]
        public ActionResult Inform()
        {
            var informs = context.Informs.SqlQuery("SELECT * FROM Inform where Status = 'False'").ToList<Inform>();
            return PartialView("_Inform", informs);
        }

        [HttpPost]
        public ActionResult Confirm(string id)
        {
            var data = context.RenterInfors.SqlQuery("SELECT * FROM RenterInfor where UserID ='" + id + "'").First<RenterInfor>();
            return Json(data);

        }
        public ActionResult Allow(string id)
        {
            var a = context.Informs.SqlQuery("SELECT * FROM Inform WHERE UserID = '" + id + "' ").First<Inform>();
            a.Status = true;
            var b = context.RenterInfors.SqlQuery("SELECT * FROM RenterInfor WHERE UserID = '" + id + "' ").First<RenterInfor>();
            b.RoomID = a.RoomID;
           
            context.SaveChanges();
            Session["Inform"] = "Allowed "+a.NameRenter+ " join !!!";
            ViewBag.Message = Session["Inform"];

            return RedirectToAction("Index","House");

        }

        // Update File and Save by ASP.net

        //[HttpPost]
        // public ActionResult UpdateFile(HttpPostedFileBase fd)
        // {
        //     if (fd != null && fd.ContentLength > 0)
        //         try
        //         {
        //             Session["ImageFile"] = fd.FileName.ToString();
        //             string path = Path.Combine(Server.MapPath("~/ImageUpdate"),
        //                                        Path.GetFileName(fd.FileName));
        //             fd.SaveAs(path);
        //             ViewBag.Message = "File uploaded successfully";
        //         }
        //         catch (Exception ex)
        //         {
        //             ViewBag.Message = "ERROR:" + ex.Message.ToString();
        //         }
        //     else
        //     {
        //         ViewBag.Message = "You have not specified a file.";
        //     }
        //     return View();
        // }


        // Update File and save many Images by AJAX

        //[HttpPost]
        //public ActionResult UploadFiles()
        //{
        //    // Checking no of files injected in Request object  
        //    if (Request.Files.Count > 0)
        //    {
        //        try
        //        {
        //            //  Get all files from Request object  
        //            HttpFileCollectionBase files = Request.Files;
        //            for (int i = 0; i < files.Count; i++)
        //            {
        //                //string path = AppDomain.CurrentDomain.BaseDirectory + "Uploads/";  
        //                //string filename = Path.GetFileName(Request.Files[i].FileName);  

        //                HttpPostedFileBase file = files[i];
        //                string fname;

        //                // Checking for Internet Explorer  
        //                if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
        //                {
        //                    string[] testfiles = file.FileName.Split(new char[] { '\\' });
        //                    fname = testfiles[testfiles.Length - 1];
        //                }
        //                else
        //                {
        //                    fname = file.FileName;
        //                }

        //                // Get the complete folder path and store the file inside it.  
        //                fname = Path.Combine(Server.MapPath("~/ImageUpdate/"), fname);
        //                file.SaveAs(fname);
        //            }
        //            // Returns message that successfully uploaded  
        //            return Json("File Uploaded Successfully!");
        //        }
        //        catch (Exception ex)
        //        {
        //            return Json("Error occurred. Error details: " + ex.Message);
        //        }
        //    }
        //    else
        //    {
        //        return Json("No files selected.");
        //    }
        //}
    }
}