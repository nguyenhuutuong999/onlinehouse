﻿using OnlineHouse.Code;
using OnlineHouse.App_Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;



namespace OnlineHouse.Controllers
{
    public class RoomController : Controller
    {
        // GET: Room
        private HouseManageDb context = new HouseManageDb();
        

        public ActionResult Index(string id)
        {
            Session["HouseID"] = id;
            if(Session["HouseID"] != null)
            {
                var model = context.Rooms.SqlQuery("select * from Room where HouseID = '" + id + "'").ToList<Room>();
                return View(model);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }

        }
        public ActionResult CreateRoom()
        {
            return View();
        }

        [HttpGet]
        public ActionResult EditRoom(int id)
        {
            var room = new RoomDao().ViewDetail(id);

            return View(room);
        }


        [HttpPost]
        public ActionResult CreateRoom(Room room)
        {
            if (ModelState.IsValid)
            {
                var dao = new RoomDao();
                room.RoomID = dao.countAscending();


                room.HouseID = (Session["HouseID"]).ToString();


                int id = dao.Insert(room);

                if (id > 0)
                {
                    return RedirectToAction("Index", "Room", new { id = (Session["HouseID"]).ToString() });
                }
                else
                {
                    ModelState.AddModelError("", "Create Error");
                }
            }
            return View("CreateRoom");
        }
        [HttpPost]
        public ActionResult EditRoom(Room room)
        {
            if (ModelState.IsValid)
            {
                var dao = new RoomDao();
                var result = dao.update(room);

                if (result)
                {
                    return RedirectToAction("Index", "Room", new { id = (Session["HouseID"]).ToString() });
                }
                else
                {
                    ModelState.AddModelError("", "Update Error");
                }
            }
            return View("EditRoom");
        }

       
        public ActionResult Delete(int id)
        {
            new RoomDao().Delete(id);
            return RedirectToAction("Index", "Room", new { id = (Session["HouseID"]) });
        }
    }
}