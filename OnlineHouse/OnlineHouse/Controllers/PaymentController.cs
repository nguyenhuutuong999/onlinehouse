﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication2;


namespace OnlineShop.Controllers
{
    public class PaymentController : Controller
    {

        public const string VPCRequest = "http://mtf.onepay.vn/onecomm-pay/vpc.op";//Url payment thật: https://onepay.vn/onecomm-pay/vpc.op 
        public const string Merchant = "ONEPAY";
        public const string AccessCode = "D67342C2";
        public const string SECURE_SECRE = "A3EFDFABA8653DF2342E8DAC29B51AF0";

        public static string ReturnURL = "http://localhost:1835/onepay/vpc_dr.aspx";
        // GET: Payment
        public ActionResult Index()
        {
            return View();
        }
        private void GuiDonHang()
        {
            string ketQua = "";

            //Lấy các thông tin người dùng gửi lên
            /* string hoTen = Request.Params["hoTen"];
             string diaChi = Request.Params["diaChi"];
             string soDienThoai = Request.Params["soDienThoai"];
             string email = Request.Params["email"];
             string phuongThucThanhToan = Request.Params["phuongThucThanhToan"];*/

            string hoTen = "Nguyen Huu Tuong";
            string diaChi = "5/48 Thuy Tu";
            string soDienThoai = "0766693571";
            string email = "nguyenhuutuong999@gmail.com";
            string phuongThucThanhToan = "Onepay";



            string mathanhtoantructuyen = DateTime.Now.Ticks.ToString();
            switch (phuongThucThanhToan)
            {

                case "Onepay":
                    #region Chuyển sang trang Onepay
                    string SECURE_SECRET = SECURE_SECRE;//Hòa: cần thanh bằng mã thật cấu hình trong app_code
                                                        // Khoi tao lop thu vien va gan gia tri cac tham so gui sang cong thanh toan
                    VPCRequest conn = new VPCRequest(VPCRequest);//Hòa: Cần thay bằng cổng thật cấu hình trong app_code
                    conn.SetSecureSecret(SECURE_SECRET);
                    // Add the Digital Order Fields for the functionality you wish to use
                    // Core Transaction Fields
                    conn.AddDigitalOrderField("Title", "onepay paygate");
                    conn.AddDigitalOrderField("vpc_Locale", "vn");//Chon ngon ngu hien thi tren cong thanh toan (vn/en)
                    conn.AddDigitalOrderField("vpc_Version", "2");
                    conn.AddDigitalOrderField("vpc_Command", "pay");
                    conn.AddDigitalOrderField("vpc_Merchant", Merchant);//Hòa: cần thanh bằng mã thật cấu hình trong app_code
                    conn.AddDigitalOrderField("vpc_AccessCode", AccessCode);//Hòa: cần thanh bằng mã thật cấu hình trong app_code
                    conn.AddDigitalOrderField("vpc_MerchTxnRef", mathanhtoantructuyen);//Hòa: mã thanh toán
                    conn.AddDigitalOrderField("vpc_OrderInfo", mathanhtoantructuyen);//Hòa: thông tin đơn hàng
                    conn.AddDigitalOrderField("vpc_Amount", (2 * 100).ToString());//Hòa: chi phí cần nhân 100 theo yêu cầu của onepay
                    conn.AddDigitalOrderField("vpc_Currency", "VND");
                    conn.AddDigitalOrderField("vpc_ReturnURL", ReturnURL);//Hòa: địa chỉ nhận kết quả trả về
                                                                          // Thong tin them ve khach hang. De trong neu khong co thong tin
                    conn.AddDigitalOrderField("vpc_SHIP_Street01", "");
                    conn.AddDigitalOrderField("vpc_SHIP_Provice", "");
                    conn.AddDigitalOrderField("vpc_SHIP_City", "");
                    conn.AddDigitalOrderField("vpc_SHIP_Country", "");
                    conn.AddDigitalOrderField("vpc_Customer_Phone", "");
                    conn.AddDigitalOrderField("vpc_Customer_Email", "");
                    conn.AddDigitalOrderField("vpc_Customer_Id", "");
                    // Dia chi IP cua khach hang
                    conn.AddDigitalOrderField("vpc_TicketNo", Request.UserHostAddress);
                    // Chuyen huong trinh duyet sang cong thanh toan
                    String url = conn.Create3PartyQueryString();
                    #endregion

                    ketQua = url;

                    break;


                    
            }
            Response.Write(ketQua);
        }
        private void CapNhatSoLuongVaoGioHang()
        {
            //Lấy id sản phẩm cần loại khỏi giỏ hàng
            string idSanPham = Request.Params["idSanPham"];
            string soLuongMoi = Request.Params["soLuongMoi"];

            //Nếu tồn tại giỏ hàng thì mới lấy ra kết quả
            if (Session["GioHang"] != null)
            {
                //Khai báo datatable để chứa giỏ hàng
                DataTable dtGioHang = new DataTable();
                dtGioHang = (DataTable)Session["GioHang"];

                //Lặp qua danh sách sản phẩm trong giỏ hàng --> Cập nhật số lượng cho sản phẩm theo id được yêu cầu
                for (int i = 0; i < dtGioHang.Rows.Count; i++)
                {
                    if (dtGioHang.Rows[i]["MaSP"].ToString() == idSanPham)
                        dtGioHang.Rows[i]["SoLuong"] = soLuongMoi;
                }

                //Gán lại vào session
                Session["GioHang"] = dtGioHang;
            }

            Response.Write("");
        }
        private string XuLyThongTinKhachHang(string hoTen, string diaChi, string soDienThoai, string email)
        {
            //Lấy danh sách khách hàng theo email --> nếu chưa có --> Thêm mới, nếu đã có thì không thực hiện gì nữa
           /* DataTable dt = KhachHang.Thongtin_Khachhang_by_emailkh(email);
            if (dt.Rows.Count == 0)
            {
                //Thêm mới khách hàng với mật khẩu chính là email của khách hàng
                string matKhau = emdepvn.MaHoa.MaHoaMD5(email);
                KhachHang.Khachang_Inser(hoTen, diaChi, soDienThoai, email, matKhau, "");

                //Thực hiện lấy lại thông tin khách hàng vừa thêm và trả về mã khách hàng
                dt = KhachHang.Thongtin_Khachhang_by_emailkh(email);
                return dt.Rows[0]["MaKH"].ToString();
            }
            else*/
                return "TS";
        }
    }
}