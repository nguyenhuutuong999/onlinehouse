﻿using OnlineHouse.App_Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OnlineHouse.Controllers
{
    public class RenterHomeController : Controller
    {
        private HouseManageDb context = new HouseManageDb();
        // GET: RenterHome
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult FilterHouse()
        {
            var model = context.Houses.SqlQuery("select * from House where Status = 'True' ").ToList<House>();
            return View(model);
        }

        public ActionResult FilterRoom(string id)
        {
            Session["HouseName"] = context.Houses.SqlQuery("select * from House where HouseID = '" + id + "'").First<House>().HouseName.ToString();

            var model = context.Rooms.SqlQuery("select * from Room where HouseID = '" + id + "'").ToList<Room>();
            return View(model);
        }
        [HttpPost]
        public ActionResult chooseRoom(string id)
        {
            Inform inform = new Inform();
            inform.RoomID = id;
            inform.UserID = Session["UserID"].ToString();
            inform.NameRenter = "Tường";
            //inform.NameRenter = Session["Name"].ToString();
            inform.HouseName = Session["HouseName"].ToString();
            //  Time
            context.Informs.Add(inform);
            context.SaveChanges();
            string message = "SUCCESS";
            return Json(new { Message = message , JsonRequestBehavior.AllowGet});
        }
    }
}