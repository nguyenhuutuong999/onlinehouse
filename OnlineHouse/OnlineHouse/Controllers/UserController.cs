﻿using OnlineHouse.Code;
using OnlineHouse.App_Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace OnlineHouse.Controllers
{
    public class UserController : Controller
    {
        private HouseManageDb _db = new HouseManageDb();
        
        
        // GET: Admin/Login
  
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Login()
        {
            return View();
        }
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(string userID, string password)
        {
            var result = new AccountModel().Login(userID, password);
            if (result && ModelState.IsValid)
            {
                Session["UserID"] = userID;
                Session["UserID"] = "toinguyen";
                Account account  = _db.Accounts.SqlQuery("select * from Account where UserID = '" + userID + "'").First<Account>();
                Session["Name"] = account.Name;
                Bill bill = _db.Bills.SqlQuery("select * from Bill where UserID = '" + userID + "'").First<Bill>();
                if (bill.Status)
                {
                    Session["BillStatus"] = "true";
                }
                else
                {
                    Session["BillStatus"] = "false";
                }

                return RedirectToAction("Index", "House");

            }
            else
            {
                ModelState.AddModelError("", "ID or Password is Invalid");
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(Account _user)
        {

            if (ModelState.IsValid)
            {
                var checkmail = _db.Accounts.FirstOrDefault(s => s.Email == _user.Email);
                var checkUserID = _db.Accounts.FirstOrDefault(s => s.UserID == _user.UserID);

                if (checkmail == null && checkUserID == null)
                {
                    //_user.Password = GetMD5(_user.Password);
                    _db.Configuration.ValidateOnSaveEnabled = false;
                    _db.Accounts.Add(_user);
                    _db.SaveChanges();
                    return RedirectToAction("Login", "User");
                }
                else
                {
                    ViewBag.error = "Email or UserID already exists";
                    return View();
                }
            }
            return View();
        }

        public ActionResult Logout()
        {
            Session.Clear();//remove session
            return RedirectToAction("Login", "User");
        }
        public static string GetMD5(string str)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] fromData = Encoding.UTF8.GetBytes(str);
            byte[] targetData = md5.ComputeHash(fromData);
            string byte2String = null;

            for (int i = 0; i < targetData.Length; i++)
            {
                byte2String += targetData[i].ToString("x2");

            }
            return byte2String;
        }



    }

}