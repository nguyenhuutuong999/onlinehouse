﻿using OnlineHouse.Code;
using OnlineHouse.App_Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;

namespace OnlineHouse.Controllers
{
    public class RenterController : Controller
    {
        // GET: DetailRoom
        private HouseManageDb context = new HouseManageDb();


        public ActionResult Index(string id)
        {
            Session["RoomID"] = id;
            if (Session["RoomID"] != null)
            {
                var RoomID = Session["RoomID"];
                var model = context.RenterInfors.SqlQuery("select * from RenterInfor where RoomID = '" + RoomID + "'").ToList<RenterInfor>();
                return View(model);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
           
        }
        [HttpGet]
        public ActionResult CreateRenter()
        {
            return View();
        }
        [HttpPost]
        public ActionResult CreateRenter(RenterInfor renter, HttpPostedFileBase ImageFile)
        {
            if (ModelState.IsValid)
            {
                var dao = new RenterDao();
                renter.RoomID = (Session["RoomID"]).ToString();
                // command upload image
                string path = Server.MapPath("~/Images");
                string fileName = Path.GetFileName(ImageFile.FileName);
                //string extension = Path.GetExtension(ImageFile.FileName);
                //fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;
                string fullPath = Path.Combine(path, fileName);
                ImageFile.SaveAs(fullPath);
                renter.ImgProfileTest = "/Images/" + fileName;

                //string fileName = Path.GetFileNameWithoutExtension(renter.ImageFile.FileName);
                //string extension = Path.GetExtension(renter.ImageFile.FileName);
                //fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;
                //renter.ImgProfileTest = "/Image/" + fileName;
                //fileName = Path.Combine(Server.MapPath("/Image/"), fileName);
                //renter.ImageFile.SaveAs(fileName);
                //int id = dao.Insert(renter);
                //ModelState.Clear();

                // end command upload image

                int id = dao.Insert(renter);
                if (id > 0)
                {
                   
                    return RedirectToAction("Index", "Renter", new { id = (Session["RoomID"]).ToString() });
                }
                else
                {
                    ModelState.AddModelError("", "Create Error");
                }
            }
            return View("CreateRenter");
        }

        [HttpGet]
        public ActionResult EditRenter(int id)
        {
            var renter = new RenterDao().ViewDetail(id);
            return View(renter);
        }
        [HttpPost]
        public ActionResult EditRenter(RenterInfor renter, HttpPostedFileBase ImageFile)
        {
            if (ModelState.IsValid)
            {
                var dao = new RenterDao();

                // Edit Images profile
                if(ImageFile != null)
                {
                    string path = Server.MapPath("~/Images");
                    string fileName = Path.GetFileName(ImageFile.FileName);
                    string fullPath = Path.Combine(path, fileName);
                    ImageFile.SaveAs(fullPath);
                    renter.ImgProfileTest = "/Images/" + fileName;
                }
                // End Edit images profile

                var result = dao.update(renter);

                if (result)
                {
                    return RedirectToAction("Index", "Renter", new { id = (Session["RoomID"]).ToString() });
                }
                else
                {
                    ModelState.AddModelError("", "Update Error");
                }
            }
            return View("EditRenter");
        }
        public ActionResult Delete(int id)
        {
            new RenterDao().Delete(id);
            return RedirectToAction("Index", "Renter", new { id = (Session["RoomID"]) });
        }
    }
}