﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="vnpay_return.aspx.cs" Inherits="OnlineHouse.vnpay_return" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head runat="server">
        <title>RETURN URL FROM VNPAY</title>
        <link href="Content/bootstrap.min.css" rel="stylesheet" />
    </head>
    <body>
        <div class="container">
            <div class="header clearfix">
                
                <h2 class="text-muted">Kết quả thanh toán</h2>
            </div>
            <div class="table-responsive">
                 <div runat="server" id="displayMsg"></div>
            </div> </div>
        <a class="btn btn-primary" href="House/Index"><< Back to Home</a>
    </body>
</html>