﻿
using OnlineHouse.App_Data;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineHouse.Code
{
    public class AccountModel
    {   
        private HouseManageDb context = null;
        public AccountModel()
        {   
            context = new HouseManageDb();
        }
      
       
        public bool Login(string userID, string password)
        {
            object[] sqlParams = 
            {
                new SqlParameter("@UserID", userID),
                new SqlParameter("@Password", password),
            };
            var res = context.Database.SqlQuery<bool>("Sp_Account_Login @UserID, @Password", sqlParams).SingleOrDefault();
            return res;
        }
    }
}
