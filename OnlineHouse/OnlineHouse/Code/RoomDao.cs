﻿
using OnlineHouse.App_Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineHouse.Code
{
    public class RoomDao
    {
        private HouseManageDb context = null;
        public RoomDao()
        {
            context = new HouseManageDb();
        }
        public int Insert(Room entity)
        {

            context.Rooms.Add(entity);
            context.SaveChanges();
            return entity.ID;
        }

        public string countAscending()
        {
            int count = context.Rooms.Count<Room>() + 1;

            return "R" + count;
        }
        public bool update(Room entity)
        {
            try
            {
                var room = context.Rooms.Find(entity.ID);
                room.Bill = entity.Bill;
                room.Capacity = entity.Capacity;
                room.Renter = entity.Renter;
                room.Standard = entity.Standard;
                context.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool Delete(int id)
        {
            try
            {
                var user = context.Rooms.Find(id);
                context.Rooms.Remove(user);
                context.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }
        public Room ViewDetail(int id)
        {
            return context.Rooms.Find(id);
        }
    }
}
