﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;
using OnlineHouse.App_Data;

namespace OnlineHouse.Code
{
    public class HouseDao
    {
        private HouseManageDb context = null;
        public HouseDao()
        {
            context = new HouseManageDb();
        }
        public int Insert(House entity)
        {

            // context.Houses.Add(entity);
            //context.SaveChanges();
            object[] sqlParams =
           {
                new SqlParameter("@HouseName", entity.HouseName),
              
            };
            context.Database.SqlQuery<bool>("House_Create @HouseName", sqlParams).SingleOrDefault();
            
            return entity.ID;
        }

        public string countAscending()
        {
            int count = context.Houses.Count<House>() + 1;

            return "H"+count;
        }
        public bool update(House entity)
        {
            try
            {
                var house = context.Houses.Find(entity.ID);
                house.HouseName = entity.HouseName;
                house.Address = entity.Address;
                house.Describe = entity.Describe; 
                house.NumberOfRoom = entity.NumberOfRoom;
                house.Image = entity.Image;
                context.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool Delete(int id)
        {
            try
            {
                var user = context.Houses.Find(id);
                context.Houses.Remove(user);
                context.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }
        public House ViewDetail(int id)
        {
            return context.Houses.Find(id);
        }
    }
}
