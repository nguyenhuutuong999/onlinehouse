﻿
using OnlineHouse.App_Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineHouse.Code
{
    public class RenterDao
    {
        private HouseManageDb context = null;
        public RenterDao()
        {
            context = new HouseManageDb();
        }
        public int Insert(RenterInfor entity)
        {
            context.RenterInfors.Add(entity);
            context.SaveChanges();
            return entity.ID;
        }

        public string countAscending()
        {
            int count = context.RenterInfors.Count<RenterInfor>() + 1;

            return "R" + count;
        }
        public bool update(RenterInfor entity)
        {
            try
            {
                var renter = context.RenterInfors.Find(entity.ID);
                renter.Fname = entity.Fname;
                renter.DayOfBirth = entity.DayOfBirth;
                renter.Sex = entity.Sex;
                renter.Address = entity.Address;
                renter.Religion = entity.Religion;
                renter.Phone = entity.Phone;
                renter.Nation = entity.Nation;
                renter.Carrer = entity.Carrer;
                renter.ImgProfileTest = entity.ImgProfileTest;
                context.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                throw e;
                //return false;
            }
        }
        public bool Delete(int id)
        {
            try
            {
                var user = context.RenterInfors.Find(id);
                context.RenterInfors.Remove(user);
                context.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }
        public RenterInfor ViewDetail(int id)
        {
            return context.RenterInfors.Find(id);
        }
    }
}
