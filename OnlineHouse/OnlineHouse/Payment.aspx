﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Payment.aspx.cs" Inherits="OnlineHouse.Payment" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Payment</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>


    <link href="https://pay.vnpay.vn/lib/vnpay/vnpay.css" rel="stylesheet" />
    <script type="text/javascript" src="https://pay.vnpay.vn/lib/vnpay/vnpay.min.js"></script>
</head>
<body>
    <div class="container" >
        <div class="" style="background:white;margin: 0 auto; width:50%;box-shadow: 10px 20px 40px rgba(0, 0, 0, .5); padding: 50px; margin-top:50px;">
            <div class="header clearfix">

                <h2 class="text-muted">Check Out</h2>
            </div>
            <div class="table-responsive">
                <form id="form1" runat="server">

                    <div class="form-group" style="">
                        <h3 id="Room" runat="server"></h3>
                        <h4>Name</h4>
                        <h4 id="Infor" runat="server"></h4>
                    </div>


                    <div class="form-group shadow-lg" style="">
                        <label>Số tiền (*)</label>
                        <table style="width: 100%" class="table">

                            <tbody style="border:1px solid #b9a4a4">
                                <tr>
                                    <th>Month</th>
                                    <td>2000000</td>
                                    <td>VND</td>
                                </tr>
                                <tr>
                                    <th>Electronic</th>
                                    <td id="Elec" runat="server"></td>
                                    <td>VND</td>
                                </tr>
                                <tr>
                                    <th>Watering</th>
                                    <td id="Water" runat="server"></td>
                                    <td>VND</td>
                                </tr>
                                <tr>
                                    <th>Cleaning</th>
                                    <td id="Clean" runat="server"></td>
                                    <td>VND</td>
                                </tr>
                                <tr >
                                    <th>
                                        <div style="font-size:30px;">Total</div>
                                    </th>
                                    <td style="font-size:30px;" id="Amount" runat="server"></td>
                                    <td style="font-size:30px;">VND</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="form-group">
                        <label>Note</label>
                        <asp:TextBox ID="OrderDescription" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <asp:Button ID="btnPay" runat="server" Text="Thanh toán" CssClass="btn btn-primary" OnClick="btnPay_Click" />

                </form>
            </div>
        </div>
    </div>


    <script type="text/javascript">

        $("#btnPayPopup").click(function () {
            var postData = $("#form1").serialize();
            var submitUrl = "<%=ResolveUrl("/GetUrl.aspx") %>";
            $.ajax({
                type: "GET",
                url: submitUrl,
                data: postData,
                dataType: 'JSON',
                success: function (x) {
                    if (x.code === '00') {
                        if (window.vnpay) {
                            vnpay.open({ width: 480, height: 600, url: x.data });
                        } else {
                            location.href = x.data;
                        }
                        return false;
                    } else {
                        alert(x.Message);
                    }
                }
            });
            return false;
        });

    </script>
</body>
</html>
