namespace OnlineHouse.App_Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Lease agreement")]
    public partial class Lease_agreement
    {
        [Key]
        [StringLength(50)]
        public string RoomID { get; set; }

        [StringLength(50)]
        public string Lessor { get; set; }

        [Column("identity card number")]
        public int? identity_card_number { get; set; }

        [Column("Phone number")]
        public int? Phone_number { get; set; }

        [StringLength(50)]
        public string Renter { get; set; }

        [StringLength(100)]
        public string Address { get; set; }

        [StringLength(50)]
        public string Phone { get; set; }

        [Column("Money/month")]
        public int? Money_month { get; set; }

        [StringLength(50)]
        public string Note { get; set; }
    }
}
