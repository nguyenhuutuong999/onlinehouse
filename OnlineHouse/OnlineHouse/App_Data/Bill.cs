namespace OnlineHouse.App_Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Bill")]
    public partial class Bill
    {
        [StringLength(50)]
        public string BillID { get; set; }

        [StringLength(50)]
        public string RoomID { get; set; }

        [StringLength(50)]
        public string UserID { get; set; }

       
        public int Electric { get; set; }

       
        public int Watering { get; set; }

      
        public int Cleaning { get; set; }

        [StringLength(50)]
        public string Sale { get; set; }

        [StringLength(50)]
        public string MonthYear { get; set; }

        public bool Status { get; set; }
    }
}
