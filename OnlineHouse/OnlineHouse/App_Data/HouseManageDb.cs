namespace OnlineHouse.App_Data
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class HouseManageDb : DbContext
    {
        public HouseManageDb()
            : base("name=HouseManageDb")
        {
        }

        public virtual DbSet<Account> Accounts { get; set; }
        public virtual DbSet<Bill> Bills { get; set; }
       
        public virtual DbSet<House> Houses { get; set; }
        public virtual DbSet<Inform> Informs { get; set; }
        public virtual DbSet<Lease_agreement> Lease_agreements { get; set; }
        public virtual DbSet<RenterInfor> RenterInfors { get; set; }
        public virtual DbSet<Room> Rooms { get; set; }
        

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
