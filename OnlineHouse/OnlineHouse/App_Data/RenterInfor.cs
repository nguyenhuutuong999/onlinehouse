namespace OnlineHouse.App_Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Web;

    [Table("RenterInfor")]
    public partial class RenterInfor
    {
        public int ID { get; set; }


        [StringLength(50)]
        public string UserID { get; set; }

        [StringLength(50)]
        public string RoomID { get; set; }

        [StringLength(50)]
        public string Fname { get; set; }

        [Column(TypeName = "date")]
        public DateTime? DayOfBirth { get; set; }

        public bool? Sex { get; set; }

        [StringLength(50)]
        public string Address { get; set; }

        [StringLength(50)]
        public string Phone { get; set; }

        [StringLength(50)]
        public string Religion { get; set; }

        [StringLength(50)]
        public string Nation { get; set; }

        [StringLength(50)]
        public string Carrer { get; set; }

        [Column(TypeName = "image")]
        public byte[] ImgProfile { get; set; }

        [Column(TypeName = "image")]
        public byte[] ImgNationID { get; set; }

        [StringLength(50)]
       
        public string Literacy { get; set; }

        [StringLength(200)]
        public string Summarize { get; set; }

        [StringLength(50)]
        public string ImgProfileTest { get; set; }

        //[DisplayName("Upload File")]
        //public string ImgProfileTest { get; set; }
        //public HttpPostedFileBase ImageFile { get; set; }
    }
}
