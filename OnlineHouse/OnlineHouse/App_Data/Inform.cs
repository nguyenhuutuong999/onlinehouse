namespace OnlineHouse.App_Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Inform")]
    public partial class Inform
    {
        [Key]
        [StringLength(50)]
        public string RoomID { get; set; }

        [StringLength(50)]
        public string UserID { get; set; }

        [StringLength(50)]
        public string NameRenter { get; set; }

        [StringLength(50)]
        public string HouseName { get; set; }

        public bool Status { get; set; }
    }
}
