namespace OnlineHouse.App_Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Room")]
    public partial class Room
    {
        public int ID { get; set; }

        [StringLength(50)]
        public string HouseID { get; set; }

        [StringLength(50)]
        public string RoomID { get; set; }

        [StringLength(50)]
        public string Renter { get; set; }

        [StringLength(50)]
        public string Bill { get; set; }

        [StringLength(50)]
        public string Standard { get; set; }

        public int? Capacity { get; set; }
    }
}
