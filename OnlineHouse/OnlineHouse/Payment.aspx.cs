﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;

using System.Linq;
using OnlineHouse.App_Data;

namespace OnlineHouse
{
    public partial class Payment : System.Web.UI.Page
    {
        private HouseManageDb context = new HouseManageDb();

        private static readonly ILog log =
          LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["UserID"] = "toinguyen";
            Bill bill = context.Bills.SqlQuery("select * from Bill where UserID = '" + Session["UserID"] + "'").First<Bill>();
            Session["BillID"] = bill.BillID;
            if (!IsPostBack)
            {
                

                Elec.InnerText = (bill.Electric * 3000).ToString();
                Water.InnerText = (bill.Watering * 5000).ToString();
                Clean.InnerText = bill.Cleaning.ToString();
                int tong = bill.Electric * 3000 + bill.Watering * 5000 + bill.Cleaning + 2000000;
                OrderDescription.Text = "Tien tro" + DateTime.Now.ToString("yyyyMMddHHmmss");
                Amount.InnerText = tong.ToString();
                Room.InnerText = "Room " + bill.RoomID + " - " + bill.MonthYear;
                Infor.InnerText = Session["Name"].ToString();
                //Infor.InnerText = "sds";
            }
        }

        protected void btnPay_Click(object sender, EventArgs e)
        {
            
            //Get Config Info
            string vnp_Returnurl = ConfigurationManager.AppSettings["vnp_Returnurl"]; //URL nhan ket qua tra ve 
            string vnp_Url = ConfigurationManager.AppSettings["vnp_Url"]; //URL thanh toan cua VNPAY 
            string vnp_TmnCode = ConfigurationManager.AppSettings["vnp_TmnCode"]; //Ma website
            string vnp_HashSecret = ConfigurationManager.AppSettings["vnp_HashSecret"]; //Chuoi bi mat

            //Get payment input
            OrderInfo order = new OrderInfo();
            //Save order to db
            order.OrderId = DateTime.Now.Ticks;
            order.Amount = Convert.ToDecimal(Amount.InnerText);
            order.OrderDescription = OrderDescription.Text;
            order.CreatedDate = DateTime.Now;

            //Build URL for VNPAY
            VnPayLibrary vnpay = new VnPayLibrary();

            vnpay.AddRequestData("vnp_Version", "2.0.0");
            vnpay.AddRequestData("vnp_Command", "pay");
            vnpay.AddRequestData("vnp_TmnCode", vnp_TmnCode);
            vnpay.AddRequestData("vnp_Locale", "vn");
            vnpay.AddRequestData("vnp_CurrCode", "VND");
            vnpay.AddRequestData("vnp_TxnRef", order.OrderId.ToString());
            vnpay.AddRequestData("vnp_OrderInfo", order.OrderDescription);
            vnpay.AddRequestData("vnp_OrderType", ""); //default value: other
            vnpay.AddRequestData("vnp_Amount", (order.Amount * 100).ToString());
            vnpay.AddRequestData("vnp_ReturnUrl", vnp_Returnurl);
            vnpay.AddRequestData("vnp_IpAddr", Utils.GetIpAddress());
            vnpay.AddRequestData("vnp_CreateDate", order.CreatedDate.ToString("yyyyMMddHHmmss"));

            string paymentUrl = vnpay.CreateRequestUrl(vnp_Url, vnp_HashSecret);
            log.InfoFormat("VNPAY URL: {0}", paymentUrl);
            Response.Redirect(paymentUrl);
        }


    }
}