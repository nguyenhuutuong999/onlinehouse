USE [QLPhongTro]
GO
/****** Object:  Table [dbo].[Account]    Script Date: 16/04/2020 8:21:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Account](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[UserID] [nvarchar](50) NULL,
	[Email] [nvarchar](50) NULL,
	[Password] [nvarchar](50) NULL,
 CONSTRAINT [PK_Account] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Bill]    Script Date: 16/04/2020 8:21:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Bill](
	[BillID] [nvarchar](50) NOT NULL,
	[RoomID] [nvarchar](50) NULL,
	[UserID] [nvarchar](50) NULL,
	[Electric] [int] NULL,
	[Watering] [int] NULL,
	[Cleaning] [int] NULL,
	[Sale] [nvarchar](50) NULL,
	[MonthYear] [nvarchar](50) NULL,
	[Status] [bit] NULL,
 CONSTRAINT [PK_Bill] PRIMARY KEY CLUSTERED 
(
	[BillID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[House]    Script Date: 16/04/2020 8:21:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[House](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[HouseID] [nvarchar](50) NULL,
	[UserID] [nvarchar](50) NULL,
	[HouseName] [nvarchar](50) NULL,
	[Address] [nvarchar](50) NULL,
	[Describe] [nvarchar](50) NULL,
	[NumberOfRoom] [nvarchar](50) NULL,
	[Image] [nvarchar](max) NULL,
	[Status] [bit] NULL,
	[NumberOfRentedRoom] [nvarchar](50) NULL,
	[Phone] [nvarchar](50) NULL,
	[NameOwer] [nvarchar](50) NULL,
 CONSTRAINT [PK_House_1] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Inform]    Script Date: 16/04/2020 8:21:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Inform](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RoomID] [nvarchar](50) NULL,
	[UserID] [nvarchar](50) NULL,
	[NameRenter] [nvarchar](50) NULL,
	[HouseName] [nvarchar](50) NULL,
	[Status] [bit] NULL,
 CONSTRAINT [PK_Inform] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lease agreement]    Script Date: 16/04/2020 8:21:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lease agreement](
	[RoomID] [nvarchar](50) NOT NULL,
	[Lessor] [nvarchar](50) NULL,
	[identity card number] [int] NULL,
	[Phone number] [int] NULL,
	[Renter] [nvarchar](50) NULL,
	[Address] [nvarchar](100) NULL,
	[Phone] [nvarchar](50) NULL,
	[Money/month] [int] NULL,
	[Note] [nvarchar](50) NULL,
 CONSTRAINT [PK_Lease agreement] PRIMARY KEY CLUSTERED 
(
	[RoomID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Place]    Script Date: 16/04/2020 8:21:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Place](
	[PlaceID] [nvarchar](50) NOT NULL,
	[Time] [date] NULL,
	[Rating] [nvarchar](50) NULL,
 CONSTRAINT [PK_Place] PRIMARY KEY CLUSTERED 
(
	[PlaceID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RenterInfor]    Script Date: 16/04/2020 8:21:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RenterInfor](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [nvarchar](50) NULL,
	[RoomID] [nvarchar](50) NULL,
	[Fname] [nvarchar](50) NULL,
	[DayOfBirth] [date] NULL,
	[Sex] [bit] NULL,
	[Address] [nvarchar](50) NULL,
	[Phone] [nvarchar](50) NULL,
	[Religion] [nvarchar](50) NULL,
	[Nation] [nvarchar](50) NULL,
	[ImgProfile] [image] NULL,
	[Carrer] [nvarchar](50) NULL,
	[ImgNationID] [image] NULL,
	[Literacy] [nvarchar](50) NULL,
	[Summarize] [nvarchar](200) NULL,
 CONSTRAINT [PK_Infor_1] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Room]    Script Date: 16/04/2020 8:21:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Room](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[HouseID] [nvarchar](50) NULL,
	[RoomID] [nvarchar](50) NULL,
	[Renter] [nvarchar](50) NULL,
	[Bill] [nvarchar](50) NULL,
	[Standard] [nvarchar](50) NULL,
	[Capacity] [int] NULL,
 CONSTRAINT [PK_Room] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Student]    Script Date: 16/04/2020 8:21:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Student](
	[studentID] [int] IDENTITY(1,1) NOT NULL,
	[studentName] [nvarchar](50) NULL,
	[studentAddress] [nvarchar](50) NULL,
	[a] [nvarchar](50) NULL,
 CONSTRAINT [PK_Student] PRIMARY KEY CLUSTERED 
(
	[studentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Account] ON 

INSERT [dbo].[Account] ([ID], [Name], [UserID], [Email], [Password]) VALUES (1, N'Tới', N'toinguyen', N'a', N'123321')
INSERT [dbo].[Account] ([ID], [Name], [UserID], [Email], [Password]) VALUES (2, N'Tường', N'1221', N'b', N'1221')
INSERT [dbo].[Account] ([ID], [Name], [UserID], [Email], [Password]) VALUES (3, N'Thiện', N'12344321', N'c', N'12344321')
INSERT [dbo].[Account] ([ID], [Name], [UserID], [Email], [Password]) VALUES (4, N'Tường', N'123321', N't', N'1233211')
INSERT [dbo].[Account] ([ID], [Name], [UserID], [Email], [Password]) VALUES (5, N'aaa', N'sa', N'a@gmail.com', N'Tuong123456789')
INSERT [dbo].[Account] ([ID], [Name], [UserID], [Email], [Password]) VALUES (6, N'tuong', N'tuong', N'nguyenhuutuong999@gmail.com', N'Tuong123')
SET IDENTITY_INSERT [dbo].[Account] OFF
INSERT [dbo].[Bill] ([BillID], [RoomID], [UserID], [Electric], [Watering], [Cleaning], [Sale], [MonthYear], [Status]) VALUES (N'1', N'R01', N'tuongnguyen', 50, 5, 15000, N'0', N'2/2020', 0)
INSERT [dbo].[Bill] ([BillID], [RoomID], [UserID], [Electric], [Watering], [Cleaning], [Sale], [MonthYear], [Status]) VALUES (N'2', N'R02', N'1221', 70, 5, 15000, N'0', N'3/2020', 0)
INSERT [dbo].[Bill] ([BillID], [RoomID], [UserID], [Electric], [Watering], [Cleaning], [Sale], [MonthYear], [Status]) VALUES (N'3', N'R03', N'12344321', 200, 2, 15000, N'0', N'3/2020', 0)
SET IDENTITY_INSERT [dbo].[House] ON 

INSERT [dbo].[House] ([ID], [HouseID], [UserID], [HouseName], [Address], [Describe], [NumberOfRoom], [Image], [Status], [NumberOfRentedRoom], [Phone], [NameOwer]) VALUES (2, N'H2', N'toinguyen', N'Hoàng Anh Gia Lai', N'254 Nguyễn Văn Linh', NULL, N'200', NULL, 0, N'50', NULL, NULL)
INSERT [dbo].[House] ([ID], [HouseID], [UserID], [HouseName], [Address], [Describe], [NumberOfRoom], [Image], [Status], [NumberOfRentedRoom], [Phone], [NameOwer]) VALUES (3, N'H3', N'toinguyen', N'Vinhome', N'100 Phạm Văn Nghị', NULL, N'1000', NULL, 1, N'500', NULL, NULL)
INSERT [dbo].[House] ([ID], [HouseID], [UserID], [HouseName], [Address], [Describe], [NumberOfRoom], [Image], [Status], [NumberOfRentedRoom], [Phone], [NameOwer]) VALUES (4, N'H4', N'1221', N'Green Star Sky Garden', N'60 Man Thiện', NULL, N'50', NULL, 1, N'20', NULL, NULL)
INSERT [dbo].[House] ([ID], [HouseID], [UserID], [HouseName], [Address], [Describe], [NumberOfRoom], [Image], [Status], [NumberOfRentedRoom], [Phone], [NameOwer]) VALUES (5, N'H5', N'toinguyen', N'Goldora Plaza Nhà Bè', N'20 Nguyen Phi KHanh', NULL, N'320', NULL, 1, N'30', NULL, NULL)
INSERT [dbo].[House] ([ID], [HouseID], [UserID], [HouseName], [Address], [Describe], [NumberOfRoom], [Image], [Status], [NumberOfRentedRoom], [Phone], [NameOwer]) VALUES (7, N'H6', N'12344321', N'Happy Home', N'11 Phạm Văn Đồng', NULL, N'150', NULL, 1, N'100', NULL, NULL)
INSERT [dbo].[House] ([ID], [HouseID], [UserID], [HouseName], [Address], [Describe], [NumberOfRoom], [Image], [Status], [NumberOfRentedRoom], [Phone], [NameOwer]) VALUES (105, N'H7', N'toinguyen', N'Duy Tân', N'45', N'124', N'100', NULL, 1, N'0', N'025465', NULL)
INSERT [dbo].[House] ([ID], [HouseID], [UserID], [HouseName], [Address], [Describe], [NumberOfRoom], [Image], [Status], [NumberOfRentedRoom], [Phone], [NameOwer]) VALUES (106, N'H7', N'toinguyen', N'tu', N'tu', N'tu', N'52', NULL, 1, N'0', N'tu', NULL)
SET IDENTITY_INSERT [dbo].[House] OFF
SET IDENTITY_INSERT [dbo].[Inform] ON 

INSERT [dbo].[Inform] ([ID], [RoomID], [UserID], [NameRenter], [HouseName], [Status]) VALUES (18, N'R10', N'tuongnguyen', N'Tường', N'Duy Tân', 1)
SET IDENTITY_INSERT [dbo].[Inform] OFF
INSERT [dbo].[Lease agreement] ([RoomID], [Lessor], [identity card number], [Phone number], [Renter], [Address], [Phone], [Money/month], [Note]) VALUES (N'P01', N'Tập Cận Bình', 26542154, 32612457, N'Giang Trạch Dân', N'wuhan', N'4598756', 1000, NULL)
INSERT [dbo].[Lease agreement] ([RoomID], [Lessor], [identity card number], [Phone number], [Renter], [Address], [Phone], [Money/month], [Note]) VALUES (N'P02', N'Hồ Cẩm Đào', 54785568, 56489762, N'Mao Trạch Đông', N'Hồ Bắc', N'4547856', 2000, NULL)
INSERT [dbo].[Lease agreement] ([RoomID], [Lessor], [identity card number], [Phone number], [Renter], [Address], [Phone], [Money/month], [Note]) VALUES (N'P03', N'Lý Khắc Cường', 54877895, 32326598, N'Đặng Tiểu Bình', N'Bắc Kinh', N'5987597', 3000, NULL)
INSERT [dbo].[Place] ([PlaceID], [Time], [Rating]) VALUES (N'01', CAST(N'2019-06-05' AS Date), N'normal')
INSERT [dbo].[Place] ([PlaceID], [Time], [Rating]) VALUES (N'02', CAST(N'2019-12-12' AS Date), N'good')
INSERT [dbo].[Place] ([PlaceID], [Time], [Rating]) VALUES (N'03', CAST(N'2018-11-03' AS Date), N'bad')
SET IDENTITY_INSERT [dbo].[RenterInfor] ON 

INSERT [dbo].[RenterInfor] ([ID], [UserID], [RoomID], [Fname], [DayOfBirth], [Sex], [Address], [Phone], [Religion], [Nation], [ImgProfile], [Carrer], [ImgNationID], [Literacy], [Summarize]) VALUES (1, N'123321', N'R1', N'Nguyễn Văn A', CAST(N'1999-08-04' AS Date), 0, N'03 Quang Trug', N'54584587', N'Không', N'Việt Nam', NULL, N'Sinh viên', NULL, N'12/12', NULL)
INSERT [dbo].[RenterInfor] ([ID], [UserID], [RoomID], [Fname], [DayOfBirth], [Sex], [Address], [Phone], [Religion], [Nation], [ImgProfile], [Carrer], [ImgNationID], [Literacy], [Summarize]) VALUES (2, N'tuongnguyen', N'R10', N'Nguyễn Hữu Tường', CAST(N'1987-03-02' AS Date), 1, N'334 Trần Cao Văn', N'26548734', N'Không', N'Lào', NULL, N'Nhân Viên Văn Phòng', NULL, N'1212', NULL)
INSERT [dbo].[RenterInfor] ([ID], [UserID], [RoomID], [Fname], [DayOfBirth], [Sex], [Address], [Phone], [Religion], [Nation], [ImgProfile], [Carrer], [ImgNationID], [Literacy], [Summarize]) VALUES (3, N'12344321', N'R1', N'Đặng Thị C', CAST(N'2018-08-04' AS Date), 1, N'64 Trường Chinh', N'54874569', N'Không', N'Việt Nam', NULL, N'Lái xe', NULL, N'12/12', NULL)
INSERT [dbo].[RenterInfor] ([ID], [UserID], [RoomID], [Fname], [DayOfBirth], [Sex], [Address], [Phone], [Religion], [Nation], [ImgProfile], [Carrer], [ImgNationID], [Literacy], [Summarize]) VALUES (4, N'12345', N'R2', N'Nguyễn Văn A', CAST(N'1999-08-04' AS Date), 0, N'03 Quang Trug', N'54584587', N'Không', N'Việt Nam', NULL, N'Sinh viên', NULL, N'12/12', NULL)
INSERT [dbo].[RenterInfor] ([ID], [UserID], [RoomID], [Fname], [DayOfBirth], [Sex], [Address], [Phone], [Religion], [Nation], [ImgProfile], [Carrer], [ImgNationID], [Literacy], [Summarize]) VALUES (5, N'12', N'R2', N'Trần Văn B', CAST(N'1987-03-02' AS Date), 0, N'334 Trần Cao Văn', N'26548734', N'Không', N'Lào', NULL, N'Nhân Viên Văn Phòng', NULL, N'1212', NULL)
INSERT [dbo].[RenterInfor] ([ID], [UserID], [RoomID], [Fname], [DayOfBirth], [Sex], [Address], [Phone], [Religion], [Nation], [ImgProfile], [Carrer], [ImgNationID], [Literacy], [Summarize]) VALUES (6, N'2121', N'R1', N'Đặng Thị C', CAST(N'2018-08-04' AS Date), 1, N'64 Trường Chinh', N'54874569', N'Không', N'Việt Nam', NULL, N'Lái xe', NULL, N'12/12', NULL)
SET IDENTITY_INSERT [dbo].[RenterInfor] OFF
SET IDENTITY_INSERT [dbo].[Room] ON 

INSERT [dbo].[Room] ([ID], [HouseID], [RoomID], [Renter], [Bill], [Standard], [Capacity]) VALUES (1, N'H1', N'R1', N'Tới', N'nợ', N'A', 1)
INSERT [dbo].[Room] ([ID], [HouseID], [RoomID], [Renter], [Bill], [Standard], [Capacity]) VALUES (3, N'H3', N'R3', N'THiện', N'payed', N'C', 2)
INSERT [dbo].[Room] ([ID], [HouseID], [RoomID], [Renter], [Bill], [Standard], [Capacity]) VALUES (4, N'H3', N'R4', N'Hạ', N'payed', N'A', 1)
INSERT [dbo].[Room] ([ID], [HouseID], [RoomID], [Renter], [Bill], [Standard], [Capacity]) VALUES (6, N'H1', N'R6', N'Trinh', N'payed', N'B', 1)
INSERT [dbo].[Room] ([ID], [HouseID], [RoomID], [Renter], [Bill], [Standard], [Capacity]) VALUES (7, N'H3', N'R7', N'Trang', N'payed', N'N', 2)
INSERT [dbo].[Room] ([ID], [HouseID], [RoomID], [Renter], [Bill], [Standard], [Capacity]) VALUES (8, N'H5', N'R8', N'Oanh', N'paye', N'E', 1)
INSERT [dbo].[Room] ([ID], [HouseID], [RoomID], [Renter], [Bill], [Standard], [Capacity]) VALUES (9, N'H2', N'R9', N'Ngân', N'payed', N'A', 2)
INSERT [dbo].[Room] ([ID], [HouseID], [RoomID], [Renter], [Bill], [Standard], [Capacity]) VALUES (11, N'H2', N'R8', N'Hoài', N'a', N'd', 2)
INSERT [dbo].[Room] ([ID], [HouseID], [RoomID], [Renter], [Bill], [Standard], [Capacity]) VALUES (12, N'H2', N'R9', N'Gấm', N'a', N'e', 1)
INSERT [dbo].[Room] ([ID], [HouseID], [RoomID], [Renter], [Bill], [Standard], [Capacity]) VALUES (15, N'H7', N'R10', N'tường', N'ttt', N'5', 5)
SET IDENTITY_INSERT [dbo].[Room] OFF
SET IDENTITY_INSERT [dbo].[Student] ON 

INSERT [dbo].[Student] ([studentID], [studentName], [studentAddress], [a]) VALUES (1, N'Tường', N'Huế', NULL)
INSERT [dbo].[Student] ([studentID], [studentName], [studentAddress], [a]) VALUES (2, N'Tới', N'Đà Nẵng', NULL)
INSERT [dbo].[Student] ([studentID], [studentName], [studentAddress], [a]) VALUES (3, N'Thiện', N'Quảng Nam', NULL)
INSERT [dbo].[Student] ([studentID], [studentName], [studentAddress], [a]) VALUES (4, N't', N't', NULL)
INSERT [dbo].[Student] ([studentID], [studentName], [studentAddress], [a]) VALUES (5, N'tt', N'tt', NULL)
INSERT [dbo].[Student] ([studentID], [studentName], [studentAddress], [a]) VALUES (6, N'a', NULL, NULL)
INSERT [dbo].[Student] ([studentID], [studentName], [studentAddress], [a]) VALUES (7, N'a', N'a', NULL)
INSERT [dbo].[Student] ([studentID], [studentName], [studentAddress], [a]) VALUES (8, N'aa', N'â', NULL)
INSERT [dbo].[Student] ([studentID], [studentName], [studentAddress], [a]) VALUES (9, N'q', N'q', NULL)
INSERT [dbo].[Student] ([studentID], [studentName], [studentAddress], [a]) VALUES (10, N'w', N'w', NULL)
SET IDENTITY_INSERT [dbo].[Student] OFF
ALTER TABLE [dbo].[House] ADD  CONSTRAINT [DF_House_NumberOfRentedRoom]  DEFAULT ((0)) FOR [NumberOfRentedRoom]
GO
ALTER TABLE [dbo].[Inform] ADD  CONSTRAINT [DF_Inform_Status]  DEFAULT ((0)) FOR [Status]
GO
/****** Object:  StoredProcedure [dbo].[House_Create]    Script Date: 16/04/2020 8:21:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[House_Create] (
	-- Add the parameters for the stored procedure here
	@HouseID nvarchar(50),
	@UserID nvarchar(50),
	@HouseName nvarchar(50),
	@Address nvarchar(50),
	@Describe nvarchar(50),
	@NumberOfRoom int,
	@Image nvarchar(MAX)
	)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	

    -- Insert statements for procedure here
	INSERT INTO House(HouseID, UserID, HouseName, Address, Describe, NumberOfRoom, Image) VALUES (@HouseID, @UserID, @HouseName, @Address, @Describe, @NumberOfRoom, @Image)
	select @HouseID
END
GO
/****** Object:  StoredProcedure [dbo].[sp_Account_Login]    Script Date: 16/04/2020 8:21:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create proc [dbo].[sp_Account_Login]
		@UserID nvarchar(50),
		@Password nvarchar(20)
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	declare @count int
	declare @res bit

	select @count = count(*) from Account where UserID = @UserID and Password = @Password

	if @count > 0
		set @res = 1
	else
		set @res = 0
    -- Insert statements for procedure here
	select @res
END
GO
